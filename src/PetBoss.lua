--- PetBoss - Pet manager
-- @author Chanticrow
-- @release 1.6

--[[
TODO

*Add keybinding to pick a random pet whenever

--]]

----------------------------------------------------------
-- Changelog

-- 0.1 - Sep 2013
--	Initial Release

-- 1.0 - 01/09/2014
--  Added Christmas Elf
--  Official Release

-- 1.1 - 01/31/2014
--  Added Tiny Tempest

-- 1.2 - 07/27/2014
--  Added Glitch, Horned Fox, Little Claw, and Baby Red Panda
--  Refactored city location detection to use game client locations instead of hardcoded locations.
--  Improved detection for currently summoned pet.

-- 1.3 - 08/24/2014
--  Fixed issue with instanceId lookup
--  Added Tame Toxic Aranha

-- 1.4 - 09/30/2014
--  Added Earthbreaker, Pet Hologram

-- 1.5 - 11/01/2014
--  Added Headless Horseman

-- 1.6 - 06/08/2015
--  Added Brontodon Court, Spirit of Flight, Vile Carcass, and Mr. Green.
--  It is a known issue that the addon may not accurately detect the ongoing presence
--  of these new pets.  The addon may continue to notify for a new random pet in these cases.
--  Refactored current pet detection to chunk out logic.
----------------------------------------------------------

require "math";
require "string";
require "table";
require "./Localization/localization";
require "lib/lib_InterfaceOptions";
require "lib/lib_Slash";
require "lib/lib_HudNote";
require "lib/lib_table";
require "lib/lib_Callback2";
require "lib/lib_Debug";
require "./lib/lib_worldLocations";

local localization_Settings = "Addon_PetBoss_Localization";

local version = "1.6";
local upgrade = {};  --manage version and upgrades

local petNotifyRunning = false;
local l_playerId = "";
local l_instanceId = "";
local l_zoneId = "";
local currentEntityId = "";
local currentPetInfo = {};
local lastSlottedItem = 0;
local lastSummonedPetInfo = {};
local PetNotify = {};
local myPetList = {};
local myPetExcludeList = {};
local myPetCount = 0;
local checkingForPet = false;
local needAPet = false;
local justSelectedRandom = false;
local remember_ExcludeLastSummonedPet = false;

----------------------------------------------------------
-- Slash Doodads
local SLASH_CMDS = {};

----------------------------------------------------------
-- Interface variables
local INTERFACE_VARS = {
	 interface_AddonEnabled = true
	,interface_NotificationFrequency = 300
	,interface_NotifyInCity = true
	,interface_CityDistance = 200
	,interface_ExcludeLastSummonedPet = false
	,interface_DebugMode = false
};

----------------------------------------------------------
-- Lists of things
-- 
-- 
-- 
-- Brontodon court info
-- http://forums.firefall.com/community/threads/pet-entire-collection.5851921/page-3

--itemTypeIds
--the text is what displays with the entity id when the pet is summoned
--the name must match the name from Game.GetCharacterTypeInfo()
--See the DoIOwnThisPet function.
local masterPetList = {
		 {itemTypeId = 76973 ,characterTypeId = 567, name = "Merch"}  --verified by ShiroSan
		,{itemTypeId = 76999 ,characterTypeId = 569, name = "Boon Boon"}		 
		,{itemTypeId = 77018 ,characterTypeId = 578, name = "Oi"}
		,{itemTypeId = 77019 ,characterTypeId = 579, name = "Nanu"}
		,{itemTypeId = 81352 ,characterTypeId = 892, name = "T.E.X."}  --no longer has an entity name
		,{itemTypeId = 85234 ,characterTypeId = 1010, name = "T.O.P. "} --no longer has an entity name
		,{itemTypeId = 85425 ,characterTypeId = 618, name = "Straw Hat Thresher"} --no longer has an entity name
		,{itemTypeId = 85958 ,characterTypeId = 1024, name = "Tiny Tempest"}
		,{itemTypeId = 85998 ,characterTypeId = 645, name = "Wintertide Elf"}
		,{itemTypeId = 96487 ,characterTypeId = 1480, name = "Little Claw"}  --no entity name
		,{itemTypeId = 96578 ,characterTypeId = 1651, name = "Baby Red Panda"} --no entity name
		,{itemTypeId = 96583 ,characterTypeId = 1654, name = "Horned Fox"}  --no entity name
		,{itemTypeId = 110768 ,characterTypeId = 1908, name = "Glitch"}
		,{itemTypeId = 118009 ,characterTypeId = 2052, name = "Tame Toxic Aranha"}
		,{itemTypeId = 118733 ,characterTypeId = 2132, name = "Earthbreaker, Pet Hologram"}
		,{itemTypeId = 118735 ,characterTypeId = 0, name = "The Royal Brontodon Court"}		
		,{itemTypeId = 118737 ,characterTypeId = 2133, name = "Headless Horseman"}
		,{itemTypeId = 120648 ,characterTypeId = 0, name = "Vile Carcass, Pet Hologram"} 
		,{itemTypeId = 121241 ,characterTypeId = 0, name = "Spirit of Flight, Pet Hologram"}
		,{itemTypeId = 121324 ,characterTypeId = 0, name = "Mr. Green Pet"} 
};

--This list defines the pets that show up on the Addon options window in game.
local optionsPetList = {
		 {itemTypeId = 76973 ,name = "Merch"}  --verified by ShiroSan
		,{itemTypeId = 77018 ,name = "Oi"}
		,{itemTypeId = 77019 ,name = "Nanu"}
		,{itemTypeId = 76999 ,name = "Boon Boon"}
		,{itemTypeId = 81352 ,name = "T.E.X."}
		,{itemTypeId = 85234 ,name = "T.O.P. "} --note the space after the name
		,{itemTypeId = 85425 ,name = "Straw Hat Thresher"}  
		,{itemTypeId = 85998 ,name = "Yule Elf"}
		,{itemTypeId = 85958 ,name = "Tiny Tempest"}
		,{itemTypeId = 96487 ,name = "Little Claw"}
		,{itemTypeId = 96578 ,name = "Baby Red Panda"}
		,{itemTypeId = 96583 ,name = "Horned Fox"}
		,{itemTypeId = 110768 ,name = "Glitch"}
		,{itemTypeId = 118009 ,name = "Tame Toxic Aranha"}
		,{itemTypeId = 118733 ,name = "Earthbreaker, Pet Hologram"}
		,{itemTypeId = 118735 ,name = "Royal Brontodon Court"}	
		,{itemTypeId = 118737 ,name = "Headless Horseman"}
		,{itemTypeId = 120648, name = "Vile Carcass, Pet Hologram"} 
		,{itemTypeId = 121241, name = "Spirit of Flight, Pet Hologram"}
		,{itemTypeId = 121324, name = "Mr. Green"} 
};
--,{itemTypeId = 118735 ,name = "Tiny Brontodon King"}

--Never auto select these pets.
local petExcludeList = { [81486] = "1-Use T.E.X."
		,[77411] = "Brontodon Mutagen NanoArmor"
		,[77412] = "Remove Brontodon Mutagen NanoArmor"
		,[77413] = "NanoArmored Mini Brontodon Leash"
		,[85585] = "1-Use Manufacturing Bot"
		,[85586] = "1-Use Manufacturing Bot Version 2"
		,[85587] = "Manufacturing Bot, Permanent"
		,[85588] = "Manufacturing Bot Version 2, Permanent"
		,[85600] = "Glowbot Pet"
		,[120634] = "Chosen Chibi Earthbreaker, Pet Hologram" 
		,[120639] = "Chosen Assault, Pet Hologram" 
		,[120640] = "Chosen Chibi Assault, Pet Hologram" 
		,[120641] = "Chosen Engineer, Pet Hologram" 
		,[120642] = "Chosen Chibi Engineer, Pet Hologram" 
		,[120643] = "Chosen Executioner, Pet Hologram" 
		,[120644] = "Chosen Chibi Executioner, Pet Hologram" 
		,[120645] = "Chosen Archon, Pet Hologram" 
		,[120646] = "Chosen Chibi Archon, Pet Hologram" 
		,[120647] = "Melded Trapjaw, Pet Hologram" 
		,[120650] = "Tiny Typhon, Pet Hologram" 
		,[120651] = "Mourningstar, Pet Hologram" 
		,[120652] = "Oilspill, Pet Hologram" 
		,[120653] = "Captain Fuller, Pet Hologram"
		,[120654] = "??" 
		,[120655] = "Chibi Typhon, Pet Hologram" 
		,[120656] = "Chibi Mourningstar, Pet Hologram"
		,[120657] = "Chibi Oilspill, Pet Hologram" 
		,[120658] = "Chibi Captain Fuller, Pet Hologram" 
		,[120659] = "??" 
		,[120719] = "Aero, Pet Hologram" 
		,[120720] = "Chibi Aero, Pet Hologram" 
		,[120925] = "Battlelab Aero, Pet Hologram" 
};

--x and y are the Manu stations since that's where most people hang out
local translatedCityLocations = {};

--[[
local cityLocations = {
	 {name = "Sunken Harbor", x = 0.0048857927322388, y = 0.10037159919739, z = 438.41510009766, chunkX = 246, chunkY = 915, type = "City", zone = 448}
	,{name = "Harbor Road", x = 0.077031254768372, y = 0.73084378242493, z = 519.79797363281, chunkX = 246, chunkY = 914, type = "Watchtower", zone = 448}
	,{name = "Sunken Pass", x = 0.37950986623764, y = 0.67635107040405, z = 531.76611328125, chunkX = 245, chunkY = 914, type = "Watchtower", zone = 448}
	,{name = "Stonewall", x = 0.76451075077057, y = 0.38376307487488, z = 559.74273681641, chunkX = 244, chunkY = 914, type = "Outpost", zone = 448}
	,{name = "Stonewall Watchtower", x = 0.018723607063293, y = 0.6609833240509, z = 570.85314941406, chunkX = 244, chunkY = 914, type = "Watchtower", zone = 448} 
	,{name = "Shanty Town", x = 0.72494900226593, y = 0.91035771369934, z = 495.62542724609, chunkX = 242, chunkY = 914, type = "Outpost", zone = 448} 
	,{name = "Broken Shores Watchtower", x = 0.42603814601898, y = 0.70439076423645, z = 586.31604003906, chunkX = 243, chunkY = 915, type = "Watchtower", zone = 448}  
	,{name = "Sigu's Sanctuary", x = 0.186926394701, y = 0.33726382255554, z = 506.45837402344, chunkX = 244, chunkY = 915, type = "Outpost", zone = 448}  
	,{name = "Biosphere Research Facility", x = 0.61622875928879, y = 0.66375231742859, z = 491.28219604492, chunkX = 244, chunkY = 915, type = "Outpost", zone = 448}  
	,{name = "Biosphere Watchtower", x = 0.28413152694702, y = 0.83790302276611, z = 468.07998657227, chunkX = 245, chunkY = 915, type = "Watchtower", zone = 448}  
	,{name = "Centauri Foray", x = 0.17325726151466, y = 0.15812993049622, z = 487.97094726563, chunkX = 244, chunkY = 916, type = "Watchtower", zone = 448}  
	,{name = "Broken Shores", x = 0.92060911655426, y = 0.3592175245285, z = 443.01507568359, chunkX = 243, chunkY = 916, type = "Outpost", zone = 448}  
	,{name = "Trans Hub Command", x = 0.39919352531433, y = 0.19596588611603, z = 468.77035522461, chunkX = 242, chunkY = 916, type = "City", zone = 448}  
	,{name = "Copacabana", x = 0.88898378610611, y = 0.18647420406342, z = 439.5485534668, chunkX = 243, chunkY = 917, type = "City", zone = 448}  
	,{name = "Praia Tropical", x = 0.92651557922363, y = 0.71678519248962, z = 488.12854003906, chunkX = 242, chunkY = 917, type = "Watchtower", zone = 448}  
	,{name = "Nutretic Processing", x = 0.24528312683105, y = 0.8334869146347, z = 534.65893554688, chunkX = 242, chunkY = 917, type = "Outpost", zone = 448}  
	,{name = "Cerrado Plains", x = 0.45903158187866, y = 0.89947074651718, z = 556.89526367188, chunkX = 241, chunkY = 918, type = "Outpost", zone = 448}  
	,{name = "Jacob's Pass", x = 0.13021731376648, y = 0.15856194496155, z = 518.15014648438, chunkX = 242, chunkY = 919, type = "Watchtower", zone = 448}  
	,{name = "Lagoa Rasa", x = 0.62759464979172, y = 0.70928275585175, z = 440.90295410156, chunkX = 244, chunkY = 918, type = "Watchtower", zone = 448}  
	 
	,{name = "Dredge", x = 0.9625928401947, y = 0.51943802833557, z = 782.72222900391, chunkX = 239, chunkY = 916, type = "City", zone = 1030} 
	,{name = "Sea of Dunes", x = 0.52413249015808, y = 0.92869853973389, z = 757.05035400391, chunkX = 240, chunkY = 917, type = "Watchtower", zone = 1030}  
	,{name = "Andreev Station", x = 0.31466233730316, y = 0.38117974996567, z = 745.82977294922, chunkX = 240, chunkY = 919, type = "Outpost", zone = 1030} 
	,{name = "Forward Operating Base Sagan", x = 0.093866378068924, y = 0.037367582321167, z = 758.5107421875, chunkX = 238, chunkY = 918, type = "Outpost", zone = 1030}
	,{name = "Tecumseh Airbase", x = 0.23383665084839, y = 0.53654915094376, z = 784.67297363281, chunkX = 236, chunkY = 918, type = "Outpost", zone = 1030} 
	,{name = "Orbital Comm Tower", x = 0.34485995769501, y = 0.23692828416824, z = 926.74908447266, chunkX = 238, chunkY = 919, type = "Outpost", zone = 1030} 
	,{name = "The Crown", x = 0.17889165878296, y = 0.33505892753601, z = 813.06097412109, chunkX = 237, chunkY = 919, type = "Watchtower", zone = 1030} 
	,{name = "Lab 16", x = 0.222039103508, y = 0.50758028030396, z = 782.27825927734, chunkX = 237, chunkY = 920, type = "Outpost", zone = 1030} 
	,{name = "The Nest", x = 0.74645483493805, y = 0.77998685836792, z = 758.47741699219, chunkX = 238, chunkY = 920, type = "Outpost", zone = 1030}  
	,{name = "FOB Harpoon", x = 0.085807085037231, y = 0.54898536205292, z = 106.75392150879, chunkX = 1161, chunkY = 1495, type = "Outpost", zone = 162}  
	,{name = "Mining Camp", x = 0.72045516967773, y = 0.89509725570679, z = 90.355918884277, chunkX = 1161, chunkY = 1494, type = "Outpost", zone = 162}  
	,{name = "Kaimuki Research Station", x = 0.44654202461243, y = 0.043237417936325, z = 85.73511505127, chunkX = 1161, chunkY = 1496, type = "Outpost", zone = 162}  
	 
	 
};

local zoneList = {
	 {zoneId = 448, name = "New Eden"}
	,{zoneId = 1030, name = "Sertao"}
	,{zoneId = 162, name = "Devil's Tusk"}
	,{zoneId = 0, name = "Broken Peninsula"}
};
--]]
----------------------------------------------------------
-- START UP AND INITIALIZATION
----------------------------------------------------------

local function SetPlayerId()
	l_playerId = Player.GetTargetId();
end

----------------------------------------------------------
local function SetInstanceId()
	--l_instanceId = Chat.GetInstanceID();
	l_instanceId = tostring(Chat.WriteInstanceKey());
end

----------------------------------------------------------
local function SetZoneId()
	l_zoneId = tostring(Game.GetZoneId());
	Debug.Log("Petboss Says: You are now on zone "..l_zoneId);
	return l_zoneId;
end

----------------------------------------------------------
local function SetDebugging(arg)
	if arg == true then
		Debug.EnableLogging(true);
	else
		Debug.EnableLogging(false);
	end
	log("Debugging is enabled? "..tostring(Debug.IsLoggingEnabled()));
end

----------------------------------------------------------
local function print_system_message(message)
	Component.GenerateEvent("MY_SYSTEM_MESSAGE", {text=message})
end

----------------------------------------------------------
local function IsInsideCircle(x_center, y_center, radius, checkX, checkY)

	local distance = (math.sqrt(math.pow((checkX - x_center), 2) + math.pow((checkY - y_center), 2))) + 2
	--log("Distance: "..tostring(distance).." and Radius: "..radius);
	
	if radius == 0 then
		return {isInCircle = true, distance = distance};
	end
	
	if distance < radius then
		return {isInCircle = true, distance = distance};
	else
		return {isInCircle = false, distance = distance};
	end
end

----------------------------------------------------------
local function SetEnable(enableFlag)
	log("Setting enabled state to: "..tostring(enableFlag));
	INTERFACE_VARS.interface_AddonEnabled = enableFlag;
end

----------------------------------------------------------
local function TranslateCityCoords()
	--Debug.Table("cityLocations", cityLocations);
	local tempCoords = {};
	local tempCityList = WORLDLOC.GetCityLocations();
	
	--[[
	for name, coords in pairs(cityLocations) do
		tempCoords = Game.ChunkCoordToWorld(coords.chunkX,coords.chunkY,coords.x,coords.y,coords.z)
		Debug.Table("tempCoords", tempCoords);
		translatedCityLocations[name] = tempCoords;
	end
	--]]
	for idx, cityInfo in pairs(tempCityList) do
		Debug.Table(cityInfo.details)
		tempCoords = {x = cityInfo.details.position.x, y = cityInfo.details.position.y, z = cityInfo.details.position.z};
		translatedCityLocations[cityInfo.details.name] = {};
		translatedCityLocations[cityInfo.details.name] = tempCoords;
	end

	Debug.Table("translatedCityLocations", translatedCityLocations);
end
----------------------------------------------------------
-- Pet Selection
-- -------------------------------------------------------

----------------------------------------------------------
local function NearACity()
	if INTERFACE_VARS.interface_AddonEnabled == false then
		return false;  --return false if the addon is disabled. Don't want to use CPU calculating distances.
	end

	if INTERFACE_VARS.interface_NotifyInCity == false then
		return true;   --just return true if they don't care about being near a city
	end

	local myPos = Player.GetPosition();
	Debug.Table("My position is: ", myPos);
	local result = {};
	for name, coords in pairs(translatedCityLocations) do
		Debug.Log("Am I near city: "..name.." at coords "..tostring(coords));
		Debug.Log("The x", coords.x);
		result = IsInsideCircle(coords.x, coords.y, INTERFACE_VARS.interface_CityDistance, myPos.x, myPos.y);
		Debug.Table("Survey says: ", result);
		if result.isInCircle == true then
			return true;
		end
	end
	return false;
end


----------------------------------------------------------
local function GetSlottedCalldownID()
	Debug.Table("Player.GetAbilities()", Player.GetAbilities());
	return Player.GetAbilities().action1.itemTypeId;
end

----------------------------------------------------------
local function RecordSummonedPet(itemInfo)
	Debug.Table("Recording summoned pet info.", itemInfo);
	currentPetInfo = itemInfo;
	lastSummonedPetInfo = itemInfo;
end

----------------------------------------------------------
local function ClearJustSelectedFlag()
	justSelectedRandom = false;
end

----------------------------------------------------------
local function SelectAPet()
	if Player.IsReady() then
		Debug.Table("My pet exclude list is: ", myPetExcludeList);
		local randomPet = 999;
		local randomCount = 0;
		while randomPet == 999 do
			randomPet = math.random(0, myPetCount - 1);
			Debug.Log("My random number is: ", randomPet);
			Debug.Table("My random pet is: ", myPetList[randomPet]);
			if myPetExcludeList[myPetList[randomPet].itemTypeId] ~= nil then
				Debug.Log("Oh no, that pet is excluded.  Try again!");
				randomPet = 999;
			elseif INTERFACE_VARS.interface_ExcludeLastSummonedPet == true then
					if lastSummonedPetInfo.itemTypeId == myPetList[randomPet].itemTypeId then
						Debug.Log("Oh no, that was the last pet I summoned.  Try again!");
						randomPet = 999;
					end
			end
			randomCount = randomCount + 1;
			if randomCount > 200 then
				randomPet = 1000;  --if no pets are included we want to avoid an infinite loop
			end
		end

		if randomPet < 999 then
			lastSlottedItem = GetSlottedCalldownID();
			Player.SlotTech(myPetList[randomPet].itemTypeId, myPetList[randomPet].itemTypeId);
		end
	end
	if PetNotify.Remove ~= nil then
		PetNotify:Remove()
	end

	justSelectedRandom = true;
	Callback2.FireAndForget(ClearJustSelectedFlag, nil, 15)
end

----------------------------------------------------------
local function WasThisAbilityAPet(args)
	--Debug.Table("Was this ability a pet?", args)
	
	for i, itemInfo in pairs(myPetList) do
		if tostring(args.id) == tostring(itemInfo.abilityId) then
			RecordSummonedPet(itemInfo);
			Debug.Table("It was a pet!", itemInfo);
		end
	end	
end

----------------------------------------------------------
local function DoIOwnThisPet(itemTypeId)
	--Debug.Log("Checking this pet ownership:", itemTypeId);
	for i, itemInfo in pairs(myPetList) do
		--Debug.Table("Against this item:", itemInfo);
		if tostring(itemInfo.itemTypeId) == tostring(itemTypeId) then
			return true;
		end
	end
	return false;
end

----------------------------------------------------------
local function CreateMyPetList()
	local consumables = Player.GetConsumableItems();	-- consumables[i] = {abilityId, itemId, itemTypeId}
	local itemInfo = {};
	local tempPetList = {};  --hold a unique list of pets because there can be dups in inventory
	for i, v in pairs(consumables) do
		itemInfo = Game.GetItemInfoByType(v.itemTypeId);
		if itemInfo.uiCategory == "PETS" then
			Debug.Table(v);
			Debug.Table(itemInfo);
			tempPetList[v.itemTypeId] = itemInfo;
		end
	end

	for i, v in pairs(tempPetList) do
		myPetList[myPetCount] = v;
		myPetCount = myPetCount + 1;
	end
end

----------------------------------------------------------
local function SortPetOptionsList()
	Debug.Log("Sorting pet option selection.");
	table.sort(optionsPetList, function(A, B) 
		Debug.Log("Sorting ", A.name, " vs ", B.name)
		return A.name < B.name;
	end);
	Debug.Log("Options Pet List is "..tostring(optionsPetList));
end

----------------------------------------------------------
local function ReturnToLastItem(args)
	Debug.Table("Returning to last item summoned.", args);
	--Debug.Table("ReturnToLastItem2", currentPetInfo);
	if justSelectedRandom == true then
		if tostring(args.id) == tostring(currentPetInfo.abilityId) then
			Player.SlotTech(lastSlottedItem, lastSlottedItem);
			justSelectedRandom = false;
		end
	end
end

----------------------------------------------------------
local function CheckForPet()
	if currentPetInfo == nil then
		needAPet = true;
		--print_system_message("No pet info.  You need a pet!");
	else
		needAPet = false;
	end

	if needAPet == false then
		Debug.Table("Current entity id:", currentEntityId);
		local TargetInfo = Game.GetTargetInfo(currentEntityId);
		if TargetInfo == nil then
			needAPet = true;
			--print_system_message("Pet entity lookup failed. You need a pet!");
		end
	end

	Callback2.FireAndForget(CheckForPet, nil, 30)
end

----------------------------------------------------------
local function SavePetEntity(entityId)
	if entityId ~= nil then  
		Debug.Log("Setting entity id", entityId);
		currentEntityId = entityId;
	end
end

----------------------------------------------------------
local function TargetIsPet(TargetInfo)
-- This function relies on a lookup table, masterPetList, in order to determine if the target
-- is a valid pet.  Unfortunately, there is nothing tying the pet ability activiation to the
-- pet entity that is summoned so I just examine everything the player's reticle sweeps over
-- and compare it to the list.
--
-- Right now the function compares the target's name to the name in the list.  It would be 
-- just as easy to use the charId instead, but I cannot find a reliable way to determine
-- the charId of all the different pets.  At the moment I have to find someone with the pet
-- in the wild so I can examine it and get the accurate name and charId.
    Debug.Log("Checking to see if this target is a pet.")
    local result = false
    
	if TargetInfo.type == "character" then
		local CharacterInfo = Game.GetCharacterTypeInfo(TargetInfo.characterTypeId);
		Debug.Log("<<////PETBOSS says: This ability might be a pet: "..tostring(TargetInfo).." --------- "..tostring(CharacterInfo).." \\\\>>");
		for index, petInfo in pairs(masterPetList) do
			if CharacterInfo.name == petInfo.name then   
			    result = true
			end
		end
	end
	
	return result
end

----------------------------------------------------------
local function IOwnTarget(ownerId)
    Debug.Log("Checking to see if I own this target.  Target: "..tostring(ownerId)..", Player: "..tostring(l_playerId));
    local result = false;
    if tostring(ownerId) == tostring(l_playerId) then
        Debug.Log("I do own this target!")
        result = true;
	end
	
	return result;
end

----------------------------------------------------------
-- Notification Handling
-- -------------------------------------------------------

----------------------------------------------------------
local function AbandonNotify()
	PetNotify:Remove();
end


----------------------------------------------------------
local function NotifyYouNeedAPet()
	petNotifyRunning = true;

	local nearCity = NearACity();

	if needAPet == true and nearCity == true and myPetCount > 0 then
		PetNotify = HudNote.Create();
		PetNotify:SetTitle(Get_Localization_Value("NOTIFY_SELECTPET_TITLE"));
		PetNotify:SetPrompt(1, Get_Localization_Value("NOTIFY_SELECTPET_YES"), SelectAPet);
		PetNotify:SetPrompt(2, Get_Localization_Value("NOTIFY_SELECTPET_NO"), AbandonNotify);
		PetNotify:SetIconTexture("icons", "hisser")
		PetNotify:SetTimeout(30, AbandonNotify);
		if INTERFACE_VARS.interface_AddonEnabled == true then
			PetNotify:Post({nopopup = true})
		end
	end

	Callback2.FireAndForget(NotifyYouNeedAPet, args, INTERFACE_VARS.interface_NotificationFrequency);  --don't go below 30 seconds or you'll conflict on the notify object
end

----------------------------------------------------------
-- INTERFACE HANDLING
-- -------------------------------------------------------

---------------------------------------------------------- 
local function Build_Interface_Options()

	InterfaceOptions.StartGroup({id="PETBOSS_NOTIFICATIONOPTIONS", label=Get_Localization_Value("INTERFACE_NOTIFICATIONOPTIONS")});
		InterfaceOptions.AddCheckBox({id="ADDON_ENABLE", label=Get_Localization_Value("INTERFACE_ENABLE"), default=true});
		InterfaceOptions.AddSlider({id="NOTIFICATIONFREQUENCY", label=Get_Localization_Value("INTERFACE_NOTIFICATIONFREQUENCY"), min=1, max=30, inc=1, format="%0.0f", suffix=Get_Localization_Value("INTERFACE_NOTIFICATIONFREQUENCY2"), default=5, tooltip=tostring(Get_Localization_Value("INTERFACE_NOTIFICATIONFREQUENCY_TOOLTIP"))})
		InterfaceOptions.AddCheckBox({id="NOTIFYINCITIES", label=Get_Localization_Value("INTERFACE_NOTIFYINCITIES"), default=true, tooltip=tostring(Get_Localization_Value("INTERFACE_NOTIFYINCITIES_TOOLTIP"))});
		InterfaceOptions.AddSlider({id="NOTIFYINCITIES_DISTANCE", label=Get_Localization_Value("INTERFACE_NOTIFYINCITIES_DISTANCE"), min=10, max=500, inc=10, format="%0.0f", suffix=Get_Localization_Value("INTERFACE_NOTIFYINCITIES_DISTANCE2"), default=200})
	InterfaceOptions.StopGroup();
			
	InterfaceOptions.StartGroup({id="PETBOSS_PETSELECTION", label=Get_Localization_Value("INTERFACE_PETSELECTION")});
		for index, petInfo in pairs(optionsPetList) do
			if DoIOwnThisPet(petInfo.itemTypeId) == true then
				InterfaceOptions.AddCheckBox({id="PETEXCLUDE_"..petInfo.itemTypeId, label=petInfo.name, default=true});
			else
				InterfaceOptions.AddCheckBox({id="PETEXCLUDE_"..petInfo.itemTypeId, label=petInfo.name, default=false, tooltip=Get_Localization_Value("INTERFACE_PETSELECTION_TOOLTIP_DONOTOWN")});
				InterfaceOptions.DisableOption("PETEXCLUDE_"..petInfo.itemTypeId, true);
			end
		end
		InterfaceOptions.AddCheckBox({id="EXCLUDELASTPET", label=Get_Localization_Value("INTERFACE_EXCLUDELASTPET"), default=false, tooltip=tostring(Get_Localization_Value("INTERFACE_EXCLUDELASTPET_TOOLTIP"))});
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.StartGroup({id="PETBOSS_LOCALIZATION", label=Get_Localization_Value("INTERFACE_LOCALIZATION_TITLE")});
		InterfaceOptions.AddChoiceMenu({id="LOCALIZATION", label=Get_Localization_Value("INTERFACE_LOCALIZATION"), default="English", tooltip=tostring(Get_Localization_Value("INTERFACE_LOCALIZATION_TOOLTIP"))})
		InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="English", label=Get_Localization_Value("INTERFACE_ENGLISH_LANGUAGE")})
		--InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="French", label=Get_Localization_Value("INTERFACE_FRENCH_LANGUAGE")})
		--InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="German", label=Get_Localization_Value("INTERFACE_GERMAN_LANGUAGE")})
		--InterfaceOptions.AddChoiceEntry({menuId="LOCALIZATION", val="Polish", label=Get_Localization_Value("INTERFACE_POLISH_LANGUAGE")})
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.StartGroup({id="PETBOSS_CREDITS", label=Get_Localization_Value("INTERFACE_CREDITS")});
	    InterfaceOptions.AddCheckBox({id="ADDON_ID", label="PetBoss "..tostring(version), default=false});
		InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_1", label=Get_Localization_Value("INTERFACE_CREDITS_1"), default=false})
		--InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_2", label=Get_Localization_Value("INTERFACE_CREDITS_2"), default=false})
		--InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_3", label=Get_Localization_Value("INTERFACE_CREDITS_3"), default=false})
		--InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_4", label=Get_Localization_Value("INTERFACE_CREDITS_4"), default=false})
		--InterfaceOptions.AddCheckBox({id="SURVEYOR_CREDITS_5", label=Get_Localization_Value("INTERFACE_CREDITS_5"), default=false})
	InterfaceOptions.StopGroup();
	
	InterfaceOptions.AddCheckBox({id="DEBUG_ENABLE", label=Get_Localization_Value("INTERFACE_DEBUG"), default=false});
end

---------------------------------------------------------- 
function OnInterfaceMessage(args)

	--Note: All of the following options are set and loaded after OnComponentLoad completes in the delayed startup area. 
	--None of these settings are loaded during OnComponentLoad.

	Debug.Table("On Interface: ", args)
	if args.type == "NOTIFICATIONFREQUENCY" then
		INTERFACE_VARS.interface_NotificationFrequency = tonumber(args.data) * 60
	elseif string.sub(args.type, 1, 10) == "PETEXCLUDE" then
		Debug.Log("Pet Exclude: ", string.sub(args.type, 12, 16));
		local itemTypeId = string.sub(args.type, 12, 16);
		if args.data == true then
			myPetExcludeList[itemTypeId] = nil;
		else
			myPetExcludeList[itemTypeId] = true;
		end
		
		--Find out how many pets are excluded.  If it is less than two then
		--disable the exclude last summoned pet option.  Otherwise the player will
		--never get a pet.
		local excludeCount = 0;
		for i,v in pairs(myPetExcludeList) do
			Debug.Log("Making exclude count: ", v);
			if v == true then
				excludeCount = excludeCount + 1;
			end
		end
		excludeCount = myPetCount - excludeCount + 1;
		Debug.Log("Final exclude count: ", excludeCount);
		if excludeCount < 2 then
			InterfaceOptions.DisableOption("EXCLUDELASTPET", true);
			INTERFACE_VARS.interface_ExcludeLastSummonedPet = false;
		else
			InterfaceOptions.DisableOption("EXCLUDELASTPET", false);
			INTERFACE_VARS.interface_ExcludeLastSummonedPet = remember_ExcludeLastSummonedPet;
		end
	elseif args.type == "ADDON_ENABLE" then
		if Player.IsReady() then 
			log("Interface setting enable to: "..tostring(args.data));
			SetEnable(args.data);
		end
	elseif args.type == "NOTIFYINCITIES" then
		INTERFACE_VARS.interface_NotifyInCity = args.data;
		if args.data == true then
			InterfaceOptions.DisableOption("NOTIFYINCITIES_DISTANCE", false);
		else
			InterfaceOptions.DisableOption("NOTIFYINCITIES_DISTANCE", true);
		end
	elseif args.type == "NOTIFYINCITIES_DISTANCE" then
		INTERFACE_VARS.interface_CityDistance = args.data;
	elseif args.type == "EXCLUDELASTPET" then
		INTERFACE_VARS.interface_ExcludeLastSummonedPet = args.data;
		remember_ExcludeLastSummonedPet = args.data;
	elseif args.type == "DEBUG_ENABLE" then
		INTERFACE_VARS.interface_DebugMode = args.data;
		SetDebugging(INTERFACE_VARS.interface_DebugMode);
	end
end


----------------------------------------------------------
-- SLASH COMMANDS
-- -------------------------------------------------------
-- I totally stole a lot of the slash code from OmniTrack.  Thanks, livervoids and Hanachi!

local function InitializeSlashCommands()
	LIB_SLASH.BindCallback({slash_list = "petboss, pb", description = "", func = SLASH_CMDS.cmdroot})
end



SLASH_CMDS.cmdroot = function(text)
	local option, message = text[1], text[2]
  
	if option ~= nil then
  		if not ( SLASH_CMDS[option] ) then log("["..option.."] Not Found") return nil end
  		SLASH_CMDS[option](message)
	else
		print_system_message(Get_Localization_Value("SLASH_DEFAULT"));  	
	end
end

SLASH_CMDS.help = function ()
	print_system_message(Get_Localization_Value("SLASH_HELP"));
end

SLASH_CMDS.loc = function ()
	local myPos = Player.GetPosition();	
	local chunkCoords = Game.WorldToChunkCoord(myPos.x, myPos.y, myPos.z);
	Debug.Table("My position is: ", myPos);
	Debug.Table("My coordinates are: ", chunkCoords);
	local zone = SetZoneId();
	Debug.Log(",{name = \"\", x = "..chunkCoords.x..", y = "..chunkCoords.y..", z = "..chunkCoords.z..", chunkX = "..chunkCoords.chunkX..", chunkY = "..chunkCoords.chunkY..", type = \"\", zone = "..zone.."}");
end

SLASH_CMDS.enable = function ()
	print_system_message(Get_Localization_Value("SLASH_ENABLE"));
	SetEnable(true);
end

SLASH_CMDS.disable = function ()
	print_system_message(Get_Localization_Value("SLASH_DISABLE"));
	SetEnable(false);
	if PetNotify.Remove ~= nil then
		PetNotify:Remove()
	end
end

SLASH_CMDS.random = function ()
	print_system_message(Get_Localization_Value("SLASH_RANDOM"));
	SelectAPet();
end

SLASH_CMDS.r = function ()
	print_system_message(Get_Localization_Value("SLASH_RANDOM"));
	SelectAPet();
end

SLASH_CMDS.listpets = function ()
	Debug.Log("Running list pets function");
	for i = 1, 300000, 1 do
		itemInfo = Game.GetItemInfoByType(i);
		if itemInfo ~= nil then
			if itemInfo.uiCategory == "PETS" then
				--Debug.Table(itemInfo);
				Debug.Log(",{itemTypeId = "..itemInfo.itemTypeId..", name = \""..itemInfo.name.."\"}")
				Debug.Log(",["..itemInfo.itemTypeId.."] = \""..itemInfo.name.."\"")
			end
		end
	end
end

SLASH_CMDS.getitem = function (itemTypeId)
	Debug.Log("Getting item details");
		itemInfo = Game.GetItemInfoByType(itemTypeId);
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end
end

SLASH_CMDS.getrootitem = function (itemTypeId)
	Debug.Log("Getting item details");
		itemInfo = Game.GetRootItemInfo(itemTypeId);
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end
end


local function LookupChar(charTypeId)
	itemInfo = Game.GetCharacterTypeInfo(charTypeId);

	
	return itemInfo;
end

local excludeCharList = {
	 [1017] = 1
	,[1072] = 1
	,[1073] = 1
	,[1074] = 1
	,[2036] = 1
	,[2037] = 1
};

SLASH_CMDS.listchars = function ()
	Debug.Log("Running list characters function");
	
	for i = 567, 588, 1 do
		--Debug.Log(i);
		itemInfo = LookupChar(i);
			
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end

	end
		
	--[[	
	for i = 567, 1071, 1 do
		--Debug.Log(i);
		if excludeCharList[i] ~= nil then
		else
			itemInfo = LookupChar(i);
		end
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end
	end
	
	for i = 2000, 10000, 1 do
		--Debug.Log(i);
		if excludeCharList[i] ~= nil then
		else
			success, itemInfo = pcall(LookupChar(i));
			
			if not success then
			   Debug.Log("Failed to look up "..i)
			   itemInfo = nil;
			end
		end
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end
	end	
	]]--
end

SLASH_CMDS.getchar = function (charTypeId)
	Debug.Log("Getting character details");
		itemInfo = LookupChar(charTypeId);
		if itemInfo ~= nil then
			Debug.Table(itemInfo);
		end
end

SLASH_CMDS.listworld = function()
	Debug.Table(Game.GetZoneInfo(Game.GetZoneId()));
	local worldObjects = Game.GetWorldObjectList();
	local objectDetails = {};
	local objectStatus = {};
	for index, objId in pairs(worldObjects) do
		objectDetails = Game.GetWorldObjectInfo(objId);
		objectStatus = Game.GetWorldObjectStatus(objId);
		Debug.Table(objectDetails);
		Debug.Table(objectStatus);
	end
end
----------------------------------------------------------
-- EVENT HANDLERS
-- -------------------------------------------------------
function OnComponentLoad()
	log("!!! On Component Load ")
	loadedAndReady = false;
	
	SetDebugging(INTERFACE_VARS.interface_DebugMode);
	--SetDebugging(true);

	Set_Localization_Values(nil, localization_Settings);
	
	SetPlayerId();
	
	SetInstanceId();

	TranslateCityCoords();

	log("Initialize Slash Commands")
	InitializeSlashCommands();	
	
	log("!!! On Component Load Complete")
	
	Callback2.FireAndForget(CheckForPet, nil, 3)
end

----------------------------------------------------------
function OnPlayerReady()
	log("Building player's pet list.")
	CreateMyPetList();
	
	SortPetOptionsList();

	log("Build Interface Options")
	Build_Interface_Options();		
	
	--Handle the interface options.
	log("Interface Options Setup")
	InterfaceOptions.SetCallbackFunc(function(id, val)
		OnInterfaceMessage({type=id, data=val})
	end, "PetBoss")	

	if petNotifyRunning == false then
		Callback2.FireAndForget(NotifyYouNeedAPet, args, 5);
	end
end

----------------------------------------------------------
function OnUpdateInstance()
	SetInstanceId();
	
	if Player.IsReady() then 
		OnPlayerReady();
	end
end

----------------------------------------------------------
function OnTargetAvailable(args)
-- Every time the reticle passes over an object I check to see if it is a pet
-- that I own.  If it is then I know I still have a pet in play and do not
-- need to summon another one.  
    local TargetInfo = Game.GetTargetInfo(args.entityId);  
    Debug.Table(TargetInfo)
    if TargetInfo ~= nil then
    	Debug.Log(IOwnTarget(TargetInfo.ownerId))
        if IOwnTarget(TargetInfo.ownerId) == true then
            if TargetIsPet(TargetInfo) == true then
		        SavePetEntity(args.entityId)
		    end
		end
	end
end

----------------------------------------------------------
function OnAbilityUsed(args)
	Callback2.FireAndForget(ReturnToLastItem, args, 1);

	WasThisAbilityAPet(args);
end



