--PetBoss
--English Language Resources
--0.1

local LANG_STRINGS = {	
	 INTERFACE_LOCALIZATION = "Select Language" 
	,INTERFACE_LOCALIZATION_TITLE = "Language" 
	,INTERFACE_ENGLISH_LANGUAGE = "English" 
	,INTERFACE_GERMAN_LANGUAGE = "German" 
	,INTERFACE_POLISH_LANGUAGE = "Polish"   
	,INTERFACE_FRENCH_LANGUAGE = "French"  
	,INTERFACE_LOCALIZATION_TOOLTIP = "Please use /rui after changing language." 
	,INTERFACE_NOTIFICATIONOPTIONS = "Notification Options"  
	,INTERFACE_DEBUG = "Enable Debugging Mode"
	,INTERFACE_ENABLE = "Enable"
	,INTERFACE_NOTIFICATIONFREQUENCY = "How long until next reminder?"
	,INTERFACE_NOTIFICATIONFREQUENCY2 = " min"  --minutes
	,INTERFACE_NOTIFICATIONFREQUENCY_TOOLTIP = [["Wait at least this many minutes before sending another reminder that a pet should be called."]]
	,INTERFACE_NOTIFYINCITIES = "Only notify while in a city?"
	,INTERFACE_NOTIFYINCITIES_DISTANCE = "How far from the city?"
	,INTERFACE_NOTIFYINCITIES_DISTANCE2 = "m"
	,INTERFACE_NOTIFYINCITIES_TOOLTIP = [["PetBoss uses the SIN tower location in each city as the center for determining distance.
	Petboss considers the following locations to be cities:
	Copacabana
	Thump Dump
	Sunken Harbor
	Trans Hub Command
	Dredge"]]
	,INTERFACE_PETSELECTION = "Include These Pets For Selection"
	,INTERFACE_PETSELECTION_TOOLTIP_DONOTOWN = "You do not own this pet."
	,INTERFACE_EXCLUDELASTPET = "Exclude last pet summoned."
	,INTERFACE_EXCLUDELASTPET_TOOLTIP = "Prevents the most recently summoned pet from being picked in the next random selection."
	
	,INTERFACE_CREDITS = "Credits" 
	,INTERFACE_CREDITS_1 = "Programming: Chanticrow" 

	,NOTIFY_SELECTPET_TITLE = "You need a pet!"
	,NOTIFY_SELECTPET_YES = "Pick a random pet."
	,NOTIFY_SELECTPET_NO = "No thanks."
		
	--Map Nodes
	,NODE_SURVEYOR_SHOWMARKERS = "Show/Hide All Surveyor Markers" 
	
	--Upgrade
	,UPGRADE_NOTICE_1 = "PetBoss: Upgrading your sites for version " 
	,UPGRADE_NOTICE_2 = "PetBoss: Upgrade complete." 	
	
	
	--Slash Commands
	,SLASH_DEFAULT = 'Use </petboss help> for commands.' 
	,SLASH_HELP = [["Thanks for using PetBoss!
	The following commands are available, and more options may be found in the Interface options. 
	</petboss random>, </petboss r>, or </pb r> to select a random pet.
	</petboss disable> to stop PetBoss from notifying you about pets.
	</petboss enable> to restart PetBoss notifications ."]]  
	,SLASH_DISABLE = "PetBoss Says: I will no longer ask you to select a pet."
	,SLASH_ENABLE = "PetBoss Says: Hooray! Let's call a pet!"
	,SLASH_RANDOM = "PetBoss Says: Random pet?  No problem!"
	
	--Miscellaneous	
	,DATE_FORMAT = "CCYY-MON-DD"   --2012-JAN-31
	,UNKNOWN_VALUE = "Unknown localization value." 
};

function GetStrings()	
	return LANG_STRINGS;
end