--[[
lib_worldObjects

Acquires and organizes a list of locations in the world.
Allows more fine grained control of object lists.  
]]

if WORLDLOC then
	-- only include once
	return nil
end

WORLDLOC = {};

local cityList = {
	 {name = "Trans Hub Command"}
	,{name = "Copacabana"}
	,{name = "Sunken Harbor"}
	,{name = "Thump Dump"}
	,{name = "Dredge"}
}

local zoneList = {
	 {zoneId = 448, name = "New Eden"}
	,{zoneId = 1030, name = "Sertao"}
	,{zoneId = 162, name = "Devil's Tusk"}
	,{zoneId = 0, name = "Broken Peninsula"}
};

----------------------------------------------------------
local function GetWorldObjectsFromGame(includeStatus)
	local worldObjects = Game.GetWorldObjectList();
	local output = {};
	local objectDetails = {};
	local objectStatus = {};
	local locId = "";
	
	for index, objId in pairs(worldObjects) do
		--Debug.Log(tostring(index), tostring(objId))
		locId = tostring(objId);
		objectDetails = Game.GetWorldObjectInfo(objId);
		
		--Debug.Table(objectDetails);
		output[locId] = {};
		output[locId].details = objectDetails;
		
		if includeStatus == true then
			objectStatus = Game.GetWorldObjectStatus(objId);
			Debug.Table(objectStatus);
			output[locId].status = objectStatus;
		else
			output[locId].status = {};
		end		
	end
	
	return output;
end



----------------------------------------------------------
WORLDLOC.GetMajorLocations = function(includeStatus)
	if type(includeStatus) ~= "boolean" then
		includeStatus = false;
	end 
	return GetWorldObjectsFromGame(includeStatus);
end

----------------------------------------------------------
WORLDLOC.GetCityLocations = function(includeStatus)
	--Consider cities to be major crafting and congregation sites.
	local allLocations = WORLDLOC.GetMajorLocations(includeStatus);
	local output = {};
	--Debug.Table("All locations:", allLocations);
	--Debug.Table(cityList);
	
	for index, locDetails in pairs(allLocations) do
		--Debug.Table(locDetails);
		for cityidx, cityDetails in pairs(cityList) do
			--Debug.Log(tostring(locDetails.details.name), tostring(cityDetails.name));
			if locDetails.details.name == cityDetails.name then
				output[index] = locDetails;
			end 
		end
	end
	
	return output;
end





